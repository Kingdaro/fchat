'use strict'

let execFile = require('child_process').execFile

let app = require('app')
let BrowserWindow = require('browser-window')
let globalShortcut = require('global-shortcut')

let flist = require('./flist')

require('crash-reporter').start()

let mainWindow = null
let debug = process.argv.indexOf('gulp-debug', 2) > -1

app.on('window-all-closed', () => {
  // for some reason people on mac like their processes still open on close???
  // whatever steve jobs was smoking, where can i get it
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 700,
    frame: debug,
  })
  
  mainWindow.loadUrl('file://' + __dirname + '/app/index.html')
  
  mainWindow.on('closed', () => {
    mainWindow = null
  })
  
  if (debug) {
    let client = require('electron-connect').client.create(mainWindow)
    globalShortcut.register('ctrl+shift+r', () => {
      client.sendMessage('restart')
    })
  }
})