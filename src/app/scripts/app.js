'use strict';

(function() {
  let app = angular.module('fchat', ['ngAnimate', 'ngCookies'])
  
  app.filter('capitalize', function() {
    return function(input, scope) {
      if (input !== null) {
        input = input.toLowerCase()
      }
      return input.substring(0, 1).toUpperCase() + input.substring(1)
    }
  })
  
  app.directive('autoScroll', ($timeout) => {
    return {
      restrict: 'A',
      
      link($scope, $element, $attr) {
        let elem = $element[0]
        let scrollTop = elem.scrollTop
        let scrollHeight = elem.scrollHeight
        let clientHeight = elem.clientHeight
        
        $scope.$watchCollection($attr.autoScroll, newValue => {
          if (scrollHeight - clientHeight === scrollTop) {
            elem.scrollTop = elem.scrollHeight
          }
          
          scrollTop = elem.scrollTop
          scrollHeight = elem.scrollHeight
          clientHeight = elem.clientHeight
        })
        
        $element.on('resize', event => {
          clientHeight = elem.clientHeight
          scrollHeight = elem.scrollHeight
        })
        
        $element.on('scroll', event => {
          scrollTop = elem.scrollTop
        })
      },
    }
  })
  
  app.directive('stopPropagation', () => {
    return {
      restrict: 'A',
      
      link($scope, $element, $attr) {
        $element.on('click', event => event.stopPropagation())
      },
    }
  })
  
  app.config(($httpProvider) => {
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
  })
  
  app.run((observer, $timeout) => {
    // timeout to let constructor functions initialize first
    $timeout(() => {
      observer.notify('appInit')
      
      observer.listen('serverHello', () => observer.notify('joinChannelRequest', 'RP Bar'))
    })
  })
})()
