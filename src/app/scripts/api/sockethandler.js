'use strict';

(function() {
  let clientName = 'LightningChat'
  let clientVersion = '0.0.1'
  let serverURL = 'mainEncrypted'

  let urls = {
    main: 'ws://chat.f-list.net:9722',
    mainEncrypted: 'wss://chat.f-list.net:9799',
    test: 'ws://chat.f-list.net:8722',
    testEncrypted: 'wss://chat.f-list.net:8799',
  }

  let commands = {
    ping: 'PIN',
    // {}

    identify: 'IDN',
    // {"method": "ticket", "account": string, "ticket": string, "character": string, "cname": string, "cversion": string}

    joinChannel: 'JCH',
    // {"channel": "Frontpage"}

    leaveChannel: 'LCH',
    // {"channel": "Frontpage"}

    sendMessage: 'MSG',
    // {"message": "Right, evenin'", "channel": "Frontpage"}
    
    sendPrivateMessage: 'PRI',
    // { "recipient": string, "message": string }
    
    publicChannelListRequest: 'CHA',
    // {}
    
    privateChannelListRequest: 'ORS',
    // {}
  }
  
  let characterInfo = {}
  let friendsList = []
  let ignoredList = []
  let globalOpsList = []

  angular.module('fchat').run(function($jsonRequest, $timeout, $log, $q, observer) {
    let clientCharacter = {}
    let socket
    
    let commandHandlers = {
      // identification
      IDN(data) {
        observer.notify('serverIdentify')
      },
      
      // Hello to you too, server!
      HLO({ message }) {
        observer.notify('serverHello', message)
      },

      // when we get PINGED, we gotta PONG back
      PIN(data) {
        sendCommand('ping')
      },

      // user count
      CON({ count }) {
        observer.notify('serverUserCount', count)
      },
      
      // receiving friends list
      FRL({ characters }) {
        friendsList = characters
//        $log.log(JSON.stringify(friendsList))
      },
      
      // receiving ignore list
      IGN({ characters, action }) {
        if (action === 'init') {
          ignoredList = characters
        }
      },
      
      // receiving list of global ops
      ADL({ ops }) {
        globalOpsList = ops
      },
      
      // receiving character information in massive loads
      LIS({ characters }) {
        $q((resolve, reject) => {
          characters.forEach(charInfo => registerCharacterInfo(...charInfo))
          resolve()
        })
      },
      
      // a character connected
      NLN({ identity, gender, status }) {
        let info = registerCharacterInfo(identity, gender, status)
        if (identity === clientCharacter.identity) {
          clientCharacter = info
        }
        observer.notify('serverCharacterConnected', info)
      },
      
      // a character disconnected
      FLN({ character }) {
        observer.notify('serverCharacterDisconnected', character)
        characterInfo[character] = undefined
      },

      // initial channel data (e.g. user list and such)
      ICH({ channel, users, mode }) {
        let characterList = users.map(user => characterInfo[user.identity])
        observer.notify('serverCharacterList', channel, characterList)
      },
      
      // channel description updates (and initialization)
      CDS({ channel, description }) {
        observer.notify('serverChannelDescription', channel, description)
      },

      // character joins channel
      JCH(data) {
        if (data.character.identity === clientCharacter.identity) {
          observer.notify('serverJoinChannelSelf', data.title, data.channel)
        }
        else {
          observer.notify('serverJoinChannel', data.title, data.channel, characterInfo[data.character.identity])
        }
      },
      
      // character leaves channel
      LCH(data) {
        if (data.character === clientCharacter.identity) {
          observer.notify('serverLeaveChannelSelf', data.channel)
        }
        else {
          observer.notify('serverLeaveChannel', data.channel, data.character)
        }
      },
      
      // character status update
      STA({ status, character, statusmsg }) {
        let info = characterInfo[character]
        info.status = status
        info.statusMessage = statusmsg
//        observer.notify('serverStatusChange', info)
      },
      
      // received a list of public channels
      CHA(data) {
        observer.notify('receivedChannelList', 'public', data.channels)
      },
      
      // received a list of private channels
      ORS(data) {
        observer.notify('receivedChannelList', 'private', data.channels)
      },

      // channel message
      MSG(data) {
        observer.notify(
          'serverChannelMessage',
          data.channel,
          data.message,
          characterInfo[data.character],
          getCurrentTime())
      },
      
      // private message from user
      PRI({ character, message }) {
        observer.notify('serverPrivateMessage', characterInfo[character], message, getCurrentTime())
      },
      
      // received server error
      ERR({ message, number }) {
        console.log(`Error from F-chat: ${message} (code: ${number})`)
      },
    }

    
    function noop() {} // nooooop
    
    function getCurrentTime() { return new Date(Date.now()).toLocaleTimeString() }
    
    function registerCharacterInfo(identity, gender, status, statusMessage = '') {
      return characterInfo[identity] = {
        identity,
        gender,
        status,
        statusMessage,

        friended: friendsList.indexOf(identity) > -1,
        ignored: ignoredList.indexOf(identity) > -1,
        globalOp: globalOpsList.indexOf(identity) > -1,
      }
    }

    function connect(identity) {
      socket = new WebSocket(urls[serverURL])

      socket.onopen = onopen
      socket.onmessage = onmessage
      socket.onclose = onclose
      socket.onerror = onerror

      clientCharacter.identity = identity
    }


    function onopen(event) {
      observer.notify('socketOpened')
      $log.log('socket opened')
      identify()
    }

    function onmessage(event) {
//      observer.notify('socketMessageRecieved', event.data)
//      $log.log('message recieved: ' + event.data)
      handleCommand(event.data)
    }

    function onclose(event) {
      observer.notify('socketClosed')
      $log.warn('socket closed')
    }

    function onerror(event) {
      observer.notify('socketError', JSON.stringify(event))
      $log.error('error: ' + JSON.stringify(event))
    }


    function send(data) {
      socket.send(data)
    }

    function sendCommand(commandName, data) {
//      $log.log(`Sending command: ${commandName} (${commands[commandName]}) with ${JSON.stringify(data || {})}`)
      if (data !== undefined) {
        send(commands[commandName] + ' ' + JSON.stringify(data))
      }
      else {
        send(commands[commandName])
      }
    }

    function handleCommand(str) {
      let command = str.substring(0, 3)
      let data = {}
      if (str.length > 4) {
        data = JSON.parse(str.substring(4))
      }

      ;(commandHandlers[command] || noop)(data)
    }


    function identify() {
      sendCommand('identify', {
        method: 'ticket',
        account: $jsonRequest.getAccountName(),
        ticket: $jsonRequest.getTicket(),
        character: clientCharacter.identity,
        cname: clientName,
        cversion: clientVersion,
      })
    }

    function sendMessage(message, channel) {
      sendCommand('sendMessage', { message, channel })
    }
    
    function sendPrivateMessage(message, characterInfo) {
      sendCommand('sendPrivateMessage', { recipient: characterInfo.identity, message })
    }
    
    function sendChannelListRequest(which) {
      if (which === 'public') {
        sendCommand('publicChannelListRequest')
      }
      if (which === 'private') {
        sendCommand('privateChannelListRequest')
      }
    }
    
    function sendJoinChannelRequest(channel) {
      sendCommand('joinChannel', { channel })
    }
    
    function sendLeaveChannelRequest(channel) {
      sendCommand('leaveChannel', { channel })
    }

//    observer.listen('sendMessage', sendMessage)
//    observer.listen('sendPrivateMessage', sendPrivateMessage)
//    observer.listen('characterChosen', connect)
//    observer.listen('requestedChannels', sendChannelListRequest)
//    observer.listen('joinChannelRequest', sendJoinChannelRequest)
//    observer.listen('leaveChannelRequest', sendLeaveChannelRequest)
//    observer.listen('loggedOut', () => socket.close())
  })
})()
