'use strict';

(function() {
  let ipc = require('ipc')
  
  function encodeURI (str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
      return '%' + c.charCodeAt(0).toString(16);
    })
  }
  
  function getCharacterImage(name) {
    return "https://static.f-list.net/images/avatar/" + encodeURI(name.toLowerCase()) + ".png"
  }
  
  let app = angular.module('fchat')
  
  app.factory('util', () => {
    return {
      getCharacterImage,
    }
  })
})()
