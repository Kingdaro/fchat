'use strict';

(function() {
  let app = angular.module('fchat')
  
  let registry = {}
  
  app.factory('observer', function() {
    return {
      listen() {
        let events = Array.from(arguments)
        let listener = events.pop()
        
        events.forEach(event => {
          if (angular.isUndefined(registry[event])) {
            registry[event] = []
          }

          let listeners = registry[event]

          listeners.push(listener)
          console.info(`Listening for ${event} (${listeners.length} listeners)`)
        })
      },
      
      ignore(event, listener) {
        let listeners = registry[event]
        if (angular.isDefined(listeners)) {
          let filtered = listeners.filter(thisListener => thisListener !== listener)
          if (listeners.length !== filtered.length) {
            console.info(`Ignoring ${event} (${registry[event].length} listeners)`)
            registry[event] = filtered.length > 0 ? filtered : undefined
          }
          else {
            console.warn(`Could not ignore ${event} with specified handle.`)
          }
        }
      },
      
      notify() {
        let args = Array.from(arguments)
        let event = args.shift()
        let listeners = registry[event]
        
        if (angular.isDefined(listeners) && listeners.length > 0) {
          listeners.forEach(listener => listener.apply(null, args))
        }
        else {
          console.warn(`No one is listening to ${event}. Maybe a typo?`)
        }
      },
    }
  })
})()
