'use strict';

(function() {
  let endpoints = {
    getTicket: "http://www.f-list.net/json/getApiTicket.php",
    // params: account, password
    /* result:
      {
        characters: [
          "list",
          "of",
          "character",
          "names"
        ]
        default_character: "Character Name",
        account_id: "some number",
        ticket: "ticket string",
        friends: [
          {
            source_name: "Friend Name",
            dest_name: "Your Character Name",
          },
          ...
        ],
        bookmarks: [
          {
            name: "Bookmarked Character Name",
          },
          ...
        ],
        error: "",
      }
    */
  }
  
  let jsonData = {
    ticket: undefined,
    characters: [],
    default_character: '',
    friends: [],
    bookmarks: [],
  }
  
  angular.module('fchat').factory('$jsonRequest', function($http, util) {
    return {
      login(account, password, success, failure) {
        console.log('logging in')
        
        let data = util.createRequestString({
          account,
          password,
        })

        $http.post(endpoints.getTicket, data).then(
          response => {
            if (response.data.error !== '') {
              console.log('failure to log in')
              console.log(response.data.error)
              failure(response.data.error)
            }
            else {
              console.log('stuff fetched:')
              console.log(JSON.stringify(response.data, null, 2))

              jsonData.account = account
              jsonData.ticket = response.data.ticket
              jsonData.characters = response.data.characters
              jsonData.default_character = response.data.default_character
              jsonData.friends = response.data.friends
              jsonData.bookmarks = response.data.bookmarks

              success(response.data)
            }
          },
          response => {
            failure()
          }
        )
      },
      
      getTicket() { return jsonData.ticket },
      getAccountName() { return jsonData.account },
    }
  })
})()
