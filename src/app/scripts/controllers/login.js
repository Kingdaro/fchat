'use strict';

(function() {
  let ipc = require('ipc')
  
  let app = angular.module('fchat')
  let alert = window.alert
    
  app.controller('LoginController',function($cookies, $scope, observer, util) {
    this.init = statusText => {
      this.username = $cookies.get('username') || ''
      this.password = ''

      this.working = false
      this.statusText = statusText || ''
    }
    
    this.open = () => {
      this.visible = true
    }
    
    this.close = () => {
      this.visible = false
    }
    
    this.submit = () => {
      $cookies.put('username', this.username)
      
      this.statusText = 'Working...'
      this.working = true
      
      ipc.once('login', (success, data) => {
        if (success) {
          this.statusText = 'Success!'
          this.close()
          
          observer.notify('loggedIn', data.characters, data.default_character)
        }
        else {
          this.statusText = 'Error! ' + (error || 'Could not connect to server.')
        }
        
        this.working = false
        $scope.$applyAsync()
      })
      
      ipc.send('login', this.username, this.password)
    }
    
    this.init()
    
    observer.listen('appInit', this.open)
    
    observer.listen('socketClosed', () => {
      this.init('Socket closed.')
      this.open()
    })
  })
})()
