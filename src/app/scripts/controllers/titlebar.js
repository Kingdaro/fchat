'use strict';

(function() {
  let app = angular.module('fchat')
  
  let require = window.require || angular.noop
  let remote = require('remote')
  
  app.controller('TitleBarController', function() {
    this.visible = remote !== undefined
    
    if (this.visible) {
      this.appWindow = remote.getCurrentWindow()
      
      this.minimize = event => {
        this.appWindow.minimize()
        event.preventDefault()
      }
      
      this.toggleRestore = event => {
        if (this.appWindow.isMaximized()) {
          this.appWindow.restore()
        }
        else {
          this.appWindow.maximize()
        }
        event.preventDefault()
      }
      
      this.close = event => {
        this.appWindow.close()
        event.preventDefault()
      }
    }
  })
})()