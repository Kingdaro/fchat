'use strict';

(function() {
  let app = angular.module('fchat')
  
  app.controller('ChannelListController', function($scope, observer) {
    this.visible = false
    this.channelList = []
    this.statusText = ''
    this.header = ''
    
    this.open = which => {
      this.visible = true
      this.statusText = 'Updating channel list...'
      
      if (which === 'public')       this.header = 'Public Channels'
      else if (which === 'private') this.header = 'Private Channels'
        
      $scope.$applyAsync()
    }
    
    this.close = () => {
      this.visible = false
      this.channelList = []
    }
    
    this.updateChannelList = (which, channelList) => {
      this.channelList = channelList
      this.channelList.sort((a, b) => (a.title || a.name).localeCompare(b.title || b.name))
      this.statusText = channelList.length > 0 ? '' : "Nobody's here. Spooky..."
      
      $scope.$applyAsync()
    }
    
    this.join = channel => {
      observer.notify('joinChannelRequest', channel.name)
      if (!channel.joined) {
        channel.joined = true
        channel.characters++
      }
      else {
        channel.joined = false
        channel.characters--
        observer.notify('leaveChannelRequest', channel.name)
      }
    }
    
    observer.listen('requestedChannels', this.open)
    observer.listen('receivedChannelList', this.updateChannelList)
    observer.listen('socketClosed', this.close)
  })
})()