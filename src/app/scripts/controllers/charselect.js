'use strict';

(function() {
  let app = angular.module('fchat')
  
  app.controller('CharSelectController', function($anchorScroll, $timeout, util, observer) {
    let modulo = (n, mod) => ((n % mod) + mod) % mod
    
    this.open = false
    
    this.init = (characters, defaultCharacter) => {
      this.characters = characters
      this.characters.sort((a, b) => {
        try {
          return a.localeCompare(b)
        }
        catch (err) {
          console.log(err)
          console.log(`not a string? ${ a }`)
          return true
        }
      })
      this.selectCharacter(defaultCharacter || this.characters[0])
      this.open = true    
    }
    
    this.close = () => {
      this.open = false
    }
    
    this.selectCharacter = char => {
      this.selectedCharacter = char
      this.avatarSRC = util.getCharacterImage(char)
    }
    
    this.isSelected = char => this.selectedCharacter === char
    this.getCharacterIndex = character => this.characters.indexOf(character)
    
    this.selectNext = dir => {
      let index = this.getCharacterIndex(this.selectedCharacter) + dir
      let character = this.characters[modulo(index, this.characters.length)]
      this.selectCharacter(character)
    }
    
    this.keydown = $event => {
      $event.preventDefault()
      if ($event.keyCode === 40 || $event.keyCode === 9) { // down arrow or tab
        this.selectNext(1)
      }
      if ($event.keyCode === 38 || ($event.keyCode === 9 && $event.shiftKey)) { // up arrow or shift+tab
        this.selectNext(-1)
      }
      
      $timeout(() => $anchorScroll('selected'))
    }
    
    this.submit = () => {
      observer.notify('characterChosen', this.selectedCharacter)
      this.close()
    }
    
    observer.listen('loggedIn', this.init)
    observer.listen('socketClosed', this.close)
  })
})()
