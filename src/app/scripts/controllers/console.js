'use strict';

(function() {
  let app = angular.module('fchat')
  
  app.controller('ConsoleController', function(observer) {
    this.messages = []
    this.visible = false
    
    this.open = () => {
      this.visible = true
    }
    
    this.close = () => {
      this.visible = false
    }
    
    this.log = message => {
      this.messages.push(message)
      console.log('[Console Message] ' + message)
    }
    
    this.clear = () => {
      this.messages = []
    }
    
    observer.listen('openConsole', this.open)
    observer.listen('closeConsole', this.close)
    observer.listen('serverHello', this.log)
    observer.listen('socketClosed', this.close)
    
    observer.listen(
      'serverIdentify',
      () => this.log('Successfully identified with the server.'))
    
    observer.listen(
      'serverUserCount',
      count => this.log(`There are currently ${count} users connected.`))
    
    observer.listen(
      'serverJoinChannelSelf',
      (title, channel) => this.log(`Successfully joined ${title} (${channel}).`)
    )
  })
})()
