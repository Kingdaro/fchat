'use strict';

(function() {
  let app = angular.module('fchat')
    
  let genderClasses = {
    'female': 'female',
    'male': 'male',
    'herm': 'herm',
    'male-herm': 'male-herm',
    'shemale': 'shemale',
    'cunt-boy': 'cboy',
    'transgender': 'transgender',
  }
  
  app.directive('characterColor', function() {
    return {
      restrict: 'A',
      link: (scope, element, attr) => {
        let unregister = scope.$watch(attr.characterColor, (value, prev) => {
          if (angular.isString(value)) {
            if (angular.isString(prev)) {
              element.removeClass(genderClasses[prev.toLowerCase()])
            }
            
            element.addClass(`character-color ${ genderClasses[value.toLowerCase()] || 'none' }`)
          }
        })
        
        scope.$on('$destroy', unregister)
      },
    }
  })
  
  app.controller('CharacterMenuController', function($scope, observer) {
    this.visible = false
    
    this.open = () => {
      observer.notify('characterMenuOpened')
      observer.listen('characterMenuOpened', this.close)
      this.visible = true
    }
    
    this.close = () => {
      this.visible = false
      observer.ignore('characterMenuOpened', this.close)
    }
    
    this.toggle = () => {
      if (!this.visible)
        this.open()
      else
        this.close()
    }
    
    $scope.$on('$destroy', () => {
      observer.ignore('characterMenuOpened', this.close)
    })
  })

  app.controller('ChatController', function($scope, util, observer) {
    this.identity = ''
    this.characterInfo = {}
    this.characterImage = ''
    
    this.rooms = [
//      {
//        title: 'RP Bar',
//        channel: 'RP Bar',
//        description: 'A bar where you RP!',
//        messages: [
//          { character: { identity: 'Some Guy' }, message: 'Hello!' },
//        ],
//        characters: [
//          { identity: 'Bob Smith' },
//          { identity: 'Bob Smith' },
//          { identity: 'Bob Smith' },
//        ],
//      },
    ]

    this.chatInput = ''
    this.maxMessages = 200
    this.currentRoom = {}
    
    this.init = identity => {
      this.identity = identity
      this.characterImage = util.getCharacterImage(identity)
      this.clearRooms()
    }
    
    this.clearRooms = () => this.rooms = []
    this.getRoomFromChannel = channel => this.rooms.find(room => room.type === 'channel' && room.channel === channel)
    this.isRoomSelected = room => this.currentRoom === room
    
    this.createRoomInfo = (type, title, info) => {
      let room = { type, title, messages: [] }
      
      if (type === 'channel') {
        room.channel = info.channel
        room.description = info.description
        room.characters = []
      }
      if (type === 'privateMessage') {
        room.characterInfo = info.characterInfo
      }
      
      return room
    }
    
    
    this.selectRoom = room => {
      if (angular.isDefined(room)) {
        this.currentRoom = room
      }
      else {
        this.currentRoom = {
          title: '',
          channel: '',
          description: '',
          messages: [],
          characters: [],
        }
      }
    }
    
    this.updateChannelDescription = (channel, description) => {
      this.getRoomFromChannel(channel).description = description
    }
    
    this.removeRoom = room => {
      let roomIndex = 0
      this.rooms = this.rooms.filter((currentRoom, index) => {
        if (currentRoom === room) {
          roomIndex = index
          return false
        }
        return true
      })
      
      this.selectRoom(this.rooms[roomIndex + 1] || this.rooms[roomIndex - 1])
    }
    
    this.closeRoom = room => {
      if (room.type === 'channel') {
        observer.notify('leaveChannelRequest', room.channel)
      }
      if (room.type === 'privateMessage') {
        this.removeRoom(room)
      }
    }
    
    this.leftChannel = channel => {
      this.removeRoom(this.getRoomFromChannel(channel))
    }
    
    this.updateChannelsJoined = (which, channelList) => {
      channelList.forEach(channel => {
        channel.joined = angular.isDefined(this.getRoomFromChannel(channel.name))
      })
    }
    
    
    this.addChannelRoom = (title, channel) => {
      let room = this.createRoomInfo('channel', { title, channel })
      
      this.rooms.push(room)
      this.selectRoom(room)
    }
    
    this.openPM = characterInfo => {
      let room = this.createRoomInfo('privateMessage', characterInfo.identity, { characterInfo })
      this.rooms.push(room)
//      this.selectRoom(room)
    }
    
    this.addMessage = (room, message, characterInfo, time) => {
      if (angular.isDefined(room)) {
        room.messages.push({ character: characterInfo, message, time })
      }
    }

    this.sendMessage = () => {
      if (this.chatInput.length === 0) return

      let messages = this.currentRoom.messages

      messages.push({
        character: this.characterInfo,
        message: this.chatInput,
      })

      while (messages.length > this.maxMessages) {
        messages.shift()
      }
      
      if (this.currentRoom.type === 'channel') {
        observer.notify('sendMessage', this.chatInput, this.currentRoom.channel)
      }
      if (this.currentRoom.type === 'privateMessage') {
        observer.notify('sendPrivateMessage', this.chatInput, this.currentRoom.characterInfo)
      }
      
      this.chatInput = ''
    }
    
    this.receivedChannelMessage = (channel, message, characterInfo, time) => {
      this.addMessage(this.getRoomFromChannel(channel), message, characterInfo, time)
    }
    
    this.receivedPrivateMessage = (characterInfo, message, time) => {
      let room = this.rooms.find(room => room.type === 'privateMessage' && room.characterInfo === characterInfo)
      
      if (angular.isUndefined(room)) {
        room = this.createRoomInfo('privateMessage', characterInfo.identity, { characterInfo })
        this.rooms.push(room)
      }
      
      this.addMessage(room, message, characterInfo, time)
    }
    
    this.removeCharacterFromRoom = (room, character) => {
      if (room.type === 'channel') {
        room.characters = room.characters.filter(char => char.identity !== character)
      }
    }
    
    this.updateCharacterList = (channel, characters) => {
      let room = this.getRoomFromChannel(channel)
      room.characters = characters
      this.organizeCharacterList(room)
    }
    
    this.characterJoinedChannel = (title, channel, characterInfo) => {
      let room = this.getRoomFromChannel(channel)
      room.characters.push(characterInfo)
      this.organizeCharacterList(room)
    }
    
    this.characterLeftChannel = (channel, character) => {
      let room = this.getRoomFromChannel(channel)
      this.removeCharacterFromRoom(room, character)
      this.organizeCharacterList(room)
    }
    
    this.characterDisconnected = character => {
      this.rooms.forEach(room => {
        this.removeCharacterFromRoom(room, character)
        this.organizeCharacterList(room)
      })
    }
    
    this.organizeCharacterList = room => {
      if (angular.isUndefined(room.characters)) return
        
      let conditions = [        
        // looking
        char => char.status === 'looking',
        
        // channel op
        char => char.globalOp,
        
        // friends and bookmarks
        char => char.friended,
      ]
      
      room.characters.forEach(char => {
        char.priority = -1
        for (let i = conditions.length - 1; i >= 0; i--) {
          if (conditions[i](char)) {
            char.priority = i
            return
          }
        }
      })
      
      room.characters.sort((a,b) => {
        return a.priority !== b.priority
          ? b.priority - a.priority              // sort by priority
          : a.identity.localeCompare(b.identity) // sort by character name
      })
    }
    
    
    this.openConsole = () => {
      observer.notify('openConsole')
    }
    
    this.openChannelList = which => {
      observer.notify('requestedChannels', which)
    }
    

    this.keydown = event => {
      if (event.keyCode === 13) {
        this.sendMessage()
        event.preventDefault()
      }
    }
    
    
    this.logOut = () => {
      observer.notify('loggedOut')
    }
    
    
    observer.listen('characterChosen', this.init)
    observer.listen('receivedChannelList', this.updateChannelsJoined)
    observer.listen('serverCharacterList', this.updateCharacterList)
    observer.listen('serverJoinChannel', this.characterJoinedChannel)
    observer.listen('serverLeaveChannel', this.characterLeftChannel)
    observer.listen('serverJoinChannelSelf', this.addChannelRoom)
    observer.listen('serverLeaveChannelSelf', this.leftChannel)
    observer.listen('serverChannelDescription', this.updateChannelDescription)
    observer.listen('serverCharacterDisconnected', this.characterDisconnected)
    observer.listen('serverChannelMessage', this.receivedChannelMessage)
    observer.listen('serverPrivateMessage', this.receivedPrivateMessage)
    
//    observer.listen(
//      'serverCharacterList',
//      'serverJoinChannel',
//      'serverLeaveChannel',
//      'serverChannelDescription',
//      'serverJoinChannelSelf',
//      'serverLeaveChannelSelf',
//      'serverChannelMessage',
//      'serverPrivateMessage',
//      'serverCharacterDisconnected',
//      () => $scope.$applyAsync()
//    )
    
    this.serverCharacterConnected = characterInfo => {
      if (characterInfo.identity === this.identity) {
        this.characterInfo = characterInfo
        observer.ignore('serverCharacterConnected', this.serverCharacterConnected)
      }
    }
    
    observer.listen('serverCharacterConnected', this.serverCharacterConnected)
    
    observer.listen('socketClosed', () => {
      this.clearRooms()
      this.selectRoom()
      observer.listen('serverCharacterConnected', this.serverCharacterConnected)
    })
  })
})()
