'use strict'

let app = require('app')
let ipc = require('ipc')

let Promise = require('promise')
let http = require('http')
let querystring = require('querystring')
  
app.on('ready', () => {
  ipc.on('login', (event, account, password) => {
    let query = querystring.stringify({
      account: account.toLowerCase(),
      password,
    })
    
    let options = {
      hostname: 'www.f-list.net',
      method: 'POST',
      path: '/json/getApiTicket.php',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': query.length,
      },
    }

    let request = http.request(options, response => {
      let data = ''

      response.on('data', chunk => {
        data += chunk
      })
      
      response.on('end', () => {
        let parsed = JSON.parse(data)
        if (parsed.error !== '')
          event.sender.send('login', false, parsed.error)
        else
          event.sender.send('login', true, parsed)
      })
    })

    request.on('error', error => {
      console.log('problem with request: ' + error.message)
      event.sender.send('login', false, error.message)
    })

    request.write(query)
    request.end()
  })
})

